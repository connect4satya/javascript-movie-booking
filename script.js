const container = document.querySelector(".container");
const seat = document.querySelectorAll(".row .seat:not(.occupied)");
const count = document.getElementById("count");
const total = document.getElementById("total");
const movie = document.getElementById("movie");

function _getSelectedSeat() {
  const selectedSeats = document.querySelectorAll(".row .seat.selected");
  const countSeat = selectedSeats.length;
  const seatsSelected = [...selectedSeats];

  const selectedIndex = seatsSelected.map((element) => {
    return [...seat].indexOf(element);
  });
  localStorage.setItem("Selected Index", JSON.stringify(selectedIndex));
  value = +movie.value;
  count.innerHTML = countSeat;
  total.innerHTML = countSeat * value;
}
function _addSelectedMovie(key, movievalue) {
  localStorage.setItem(key, movievalue);
}
function _getChangedMovie() {
  let movieIndex = movie.selectedIndex;
  _getSelectedSeat();
  _addSelectedMovie("movie-Index", movieIndex);
}
function _fetchSavedDetails() {
  const getSeat = JSON.parse(localStorage.getItem("Selected Index"));
  const getMovie = JSON.parse(localStorage.getItem("movie-Index"));

  // const selectedValues = [...getSeat].map((element) => {
  //   console.log("element-" + element);
  //   return [...seat][element];
  // });
  if (getSeat !== null) {
    seat.forEach((element, index) => {
      if (getSeat.indexOf(index) > -1) {
        element.classList.add("selected");
      }
    });
  }
  if (getMovie !== null) {
    movie.selectedIndex = getMovie;
  }
}
function _callMethod(e) {
  if (
    e.target.classList.contains("seat") &&
    !e.target.classList.contains("occupied")
  ) {
    e.target.classList.toggle("selected");
    _getSelectedSeat();
  }
}

container.addEventListener("click", _callMethod);
movie.addEventListener("change", _getChangedMovie);
_fetchSavedDetails();
_getSelectedSeat();
